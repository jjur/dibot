<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
    <title>Disaster Interactive Bot</title>

    <!-- CSS  -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="<?php echo base_url(); ?>/assets/css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
    <link href="<?php echo base_url(); ?>/assets/css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
</head>
<body class="bg-blue">
<ul id="dropdown1" class="dropdown-content">
    <li><a href="<?php echo base_url() ;?>/Disasters/Earthquake">Earthquake</a></li>
    <li><a href="<?php echo base_url() ;?>/Disasters/Fire">Fire</a></li>
    <li><a href="<?php echo base_url() ;?>/Disasters/Flood">Flood</a></li>
    <li><a href="<?php echo base_url() ;?>/Disasters/Hurricane">Hurricans</a></li>
</ul>
<nav class="white-text bg-blue" role="navigation">
    <div class="nav-wrapper container "><a id="logo-container" href="<?php echo base_url() ;?>/" class="brand-logo white-text"><b>DiBot</b><small> 701-638-3998</small></a>
        <ul class="right hide-on-med-and-down">
            <li><a href="<?php echo base_url() ;?>/Disasters/Kit">Kit</a></li>
            <li><a class="dropdown-trigger" href="<?php echo base_url() ;?>/" data-target="dropdown1">Emergency Information<i class="material-icons right">arrow_drop_down</i></a></li>
            <li><a href="<?php echo base_url() ;?>/Users/Plan">My Checklist</a></li>
            <li><a href="<?php echo base_url() ;?>/Welcome/Register">Register</a></li>
            <li><a href="<?php echo base_url() ;?>/Welcome/Staff">About Us</a></li>
        </ul>
        <ul id="nav-mobile" class="sidenav">
            <li><a href="<?php echo base_url() ;?>/">Home</a></li>
            <li><a href="<?php echo base_url() ;?>/Disasters/Kit">Emergency Kit</a></li>
            <li><div class="divider"></div> </li>
            <li><a href="<?php echo base_url() ;?>/Disasters/Earthquake">Earthquake</a></li>
            <li><a href="<?php echo base_url() ;?>/Disasters/Fire">Fire</a></li>
            <li><a href="<?php echo base_url() ;?>/Disasters/flood">Flood</a></li>
            <li><a href="<?php echo base_url() ;?>/Disasters/Hurricane">Hurricans</a></li>
            <li><div class="divider"></div> </li>
            <li><a href="<?php echo base_url() ;?>/Users/Plan">My Checklist</a></li>
            <li><a href="<?php echo base_url() ;?>/Welcome/Register">Register</a></li>

            <li><a href="<?php echo base_url() ;?>/Welcome/Staff">About us</a></li>

        </ul>
        <a href="#" data-target="nav-mobile" class="sidenav-trigger"><i class="material-icons white-text">menu</i></a>

    </div>
</nav>

