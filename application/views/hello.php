<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->view("header");
?>
<div class="slider">
    <ul class="slides">
        <li>
            <img src="<?php echo base_url(); ?>/assets/img/1.jpg" style="filter: brightness(70%);">
            <!-- random image -->
            <div class="caption center-align">

                <h1 class="header center">Be always prepared!</h1>
                <div class="row center">
                    <h5 class="header col s12 ">Our virual assistent on the line will always support you during any
                        emergency situation.</h5>
                </div>
                <div class="row center">
                    <a href="http://dibot.ml/Welcone/register" id="download-button"
                       class="btn-large waves-effect white-text bg-blue-light">Get Started</a>
                </div>
                <br><br>
            </div>
        </li>
        <li>
            <img src="<?php echo base_url(); ?>/assets/img/floods2.jpg" style="filter: brightness(50%);">
            <!-- random image -->
            <div class="caption center-align">
                <h5 class="light grey-text text-lighten-3">It does not matter if it is a </h5>
                <h2>Flood</h2>
            </div>
        </li>
        <li>
            <img src="<?php echo base_url(); ?>/assets/img/3.jpg" style="filter: brightness(60%);"><!-- random image -->
            <div class="caption center-align">
                <h5 class="light grey-text text-lighten-3">It does not matter if it is a </h5>
                <h2>Hurricane</h2>
            </div>
        </li>
        <li>
            <img src="<?php echo base_url(); ?>/assets/img/4.jpg" style="filter: brightness(50%);">
            <!-- random image -->

            <div class="caption center-align">
                <h5 class="light grey-text text-lighten-3">It does not matter if it is a </h5>
                <h2>or Earthquake</h2>
            </div>

        </li>
        <li>
            <img src="<?php echo base_url(); ?>/assets/img/5.jpg" style="filter: brightness(70%);">
            <!-- random image -->

            <div class="caption center-align">
                <h3>Our Asistent will be always with you</h3>
                <h4>Call: +1 701-638-3998</h4>
                <div class="row center">
                    <a href="http://dibot.ml/Welcone/register" id="download-button"
                       class="btn-large waves-effect white-text bg-blue-light">Get Started</a>
                </div>
            </div>

        </li>
    </ul>
</div>


<div class="container flow-text">
    <div class="row">


        <div class="col s12">
            <p align="justify">What would you do if you are woken up by a weird intense shaking?
                What if you are alerted
                about a hurricane and you have evacuate immediately? What if the storm was stronger than predicted and
                now you
                are stuck at home without energy because of a flood? Do you know the most frequency types of natural
                disasters
                that affect your location? What about in your business or vacation trip location? </p></div>

        <div class="col m6 s12"><br>
            <iframe src="https://appliedsciences.nasa.gov/sites/default/files/videos/original/David-ASP_July_17_Disasters_FINAL_captioned.mp4"
                    style="width: 100%; height: 400px"></iframe>
        </div>

        <div class="col m6 s12"><p align="justify">Almost one million of Americans are somehow affected by one of the
                four major types of natural
                disaster: storms
                (including hurricanes), earthquakes, wildfire, and flood. Research shows that even thought 82% of
                americans know
                the importance of being prepared to handle disasters, only 39% of them have developed a plan to deal
                with these
                situations. </p></div>

        <div class="col s12"><p align="justify">The frequency of natural disasters is increasing, and in order to have
                the best chances of
                survival, you need to
                prepare a plan of action. Being prepared for such emergencies reduces the fear and the anxiety that you
                feel
                during it, what improves your overall performance and survival rates.</p></div>

        <div class="col m6 s12"><p align="justify">One should have an action plan that covers the next 72 hours following a
                dangerous situation, in
                which he/she should be concerned about food, water, power, clothes, flashlights, and any other specific
                needs
                you have, such as medication. </p></div>

        <div class="col m6 s12"><iframe width="560" height="315" src="https://www.youtube.com/embed/TP4vzFojewU" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe></div>

        <div class="col s12"><p align="justify">Emergencies can happen anywhere and affect anyone. In our website you
                can find information about
                the most common types of natural disasters the americans face, the recommended actions you should follow
                to
                better protect yourself during an emergency, and a tool to create your own plan of actions to follow to
                better
                prepare for it.</p></div>
    </div>
</div>



<?php

$this->load->view("footer"); ?>