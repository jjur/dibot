<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->view("header");
?>
    <div class="slider">
        <ul class="slides">
            <li>
                <img src="https://images-assets.nasa.gov/image/PIA11989/PIA11989~medium.jpg?fbclid=IwAR0tKCNrcATorPK5DHAPy5bKv46cFVUNXdfIQNpqCt5wElDXmxPocfhP_kU" style="filter: brightness(70%);">
                <!-- random image -->
                <div class="caption center-align">
                    <h1 class="header center">Flood</h1>
                    <div class="row center">
                        <h5 class="header col s12 light">Learn more on flood and how to prepare for it</h5>
                    </div>
                    <br><br>
                </div>
            </li>
            <li>
                <img src="https://images-assets.nasa.gov/image/PIA00123/PIA00123~small.jpg?fbclid=IwAR0Zz-kgiriC08dxoHnqrhGQYP9PM_RJHnpTv7sA1xOjR2mla7L8eEixtpI" style="filter: brightness(50%);">
                <!-- random image -->
                <div class="caption center-align">
                    <h1 class="header center">Flood</h1>
                    <div class="row center">
                        <h5 class="header col s12 light">Learn more on flood and how to prepare for it</h5>
                    </div>
                    <br><br>
                </div>
            </li>
            <li>
                <img src="https://images-assets.nasa.gov/image/PIA00342/PIA00342~medium.jpg?fbclid=IwAR1Hhbcy8I0fk7hsdMh5Dgh5a6havHyaq_GLpOeym_JbV54uDC7PqfL7Cbs" style="filter: brightness(60%);"><!-- random image -->
                <div class="caption center-align">
                    <h1 class="header center">Flood</h1>
                    <div class="row center">
                        <h5 class="header col s12 light">Learn more on flood and how to prepare for it</h5>
                    </div>
                    <br><br>
                </div>
            </li>
        </ul>
    </div>




<div class="container">
    <div class="section">

        <!--   Icon Section   -->
        <div class="row">
            <div class="col s12 m4">

                    <h2 class="center "><img src="https://www.sciencenews.org/sites/default/files/2018/04/main/articles/040418_CG_mississippi-flooding_feat.jpg?fbclid=IwAR0vgA5WG8FLVJmg18-BMwEHmhIhZASmAhokmMZOq0GysZx9xPu_XlIFDqQ" alt="Earthquake formation" class="responsive-img"></h2>
                    <h5 class="center white-text">What is Flood?</h5>
                    <p class="light white-text"><font size="2.5">A flood is an overflow of water that submerges land that is usually dry. The European Union (EU) Floods Directive defines a flood as a covering by water of land not normally covered by water. In the sense of "flowing water", the word may also be applied to the inflow of the tide. Floods are an area of study of the discipline hydrology and are of significant concern in agriculture, civil engineering and public health.</font></p>
            </div>

            <div class="col s12 m8">

                    <h5 class="center white-text">Effects of flood?</h5>

                    <small><p class="light white-text custom-text"> The primary effects of flooding include loss of life, damage to buildings and other structures, including bridges, sewerage systems, roadways, and canals.
                            Floods also frequently damage power transmission and sometimes power generation, which then has knock-on effects caused by the loss of power.
                            Damage to roads and transport infrastructure may make it difficult to mobilize aid to those affected or to provide emergency health treatment.
                            Flood waters typically inundate farm land, making the land unworkable and preventing crops from being planted or harvested, which can lead to shortages of food both for humans and farm animals.
                        </p></small>

            </div>
        </div>
        <div class="row">
            <div class="col s12 m4">
                <h2 class="center a"><img src="https://images-assets.nasa.gov/image/PIA15008/PIA15008~medium.jpg" class="responsive-img"></h2>
            </div>

            <div class="col s12 m8">


                    <h5 class="center white-text">How to prepare for Flood?</h5>

                    <p class="light white-text custom-text" >Be alert.<br>
                            Monitor your surroundings.<br>
                            If a flash flood warning is issued for your area: Climb to safety immediately.<br>
                            If driving, do not drive through flooded roadways!<br>
                            Assemble disaster supplies.<br>
                            Identify places to go.<br>
                            Identify alternative travel routes that are not prone to flooding.<br>
                            Plan what to do with your pets.<br>
                            Fill your car’s gas tank.<br>
                            If told to leave, do so quickly.<br>
                            Review your Family Disaster Plan.<br>
                            Discuss flood plans with your family.<br>
                            Decide where you will meet if separated.<br>
                            Designate a contact person who can be reached if family members get separated. Make sure every family member has the contact information.<br>
                            Protect your property.<br>
                            Move valuables and furniture to higher levels.<br>
                            Move hazardous materials (such as paint, oil, pesticides, and cleaning supplies) to higher locations.<br>
                            Disconnect electrical appliances. Do not touch them if you are wet or standing in water.<br>
                            Bring outside possessions indoors or tie them down securely. This includes lawn furniture, garbage cans, and other movable objects.<br>
                            Seal vents to basements to prevent flooding.</font></p>
                </div>
            </div>
            </div>
        <div class="row">
            <h6 align="left"><p>


                    <a  class="btn bg-blue-light" href="https://www.ready.gov/floods">Get more information about how to act in case of floods</a>

                </p>
            </h6>
        </div>
        </div>

    </div>
    <br><br>
</div>


<?php

$this->load->view("footer"); ?>