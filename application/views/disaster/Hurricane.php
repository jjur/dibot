<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->view("header");
?>


    <div class="slider">
        <ul class="slides">
            <li>
                <img src="https://images-assets.nasa.gov/image/STS106-704-063/STS106-704-063~medium.jpg?fbclid=IwAR2V_3zlPwwNQzoWkD0kjOwCFrtVf2zgBw9o6GEsSVry0pjwxqRDHsXFs-w" style="filter: brightness(70%);">
                <!-- random image -->
                <div class="caption center-align">

                    <h1 class="header center">Hurricane</h1>
                    <div class="row center">
                        <h5 class="header col s12 light">Learn more on hurricane and how to prepare for it</h5>
                    </div>
                    <br><br>
                </div>
            </li>
            <li>
                <img src="https://images-assets.nasa.gov/image/PIA20898/PIA20898~medium.jpg?fbclid=IwAR1DlOnLOttMxjvEyzgQcGKOrIxITBjjUFfG_XujN8VpyoTs0ddj8jZuMTw" style="filter: brightness(50%);">
                <!-- random image -->
                <div class="caption left-align">
                    <h5 class="light grey-text text-lighten-3">Learn more on hurricane and how to prepare for it </h5>
                </div>
            </li>
            <li>
                <img src="https://images-assets.nasa.gov/image/iss011e12343/iss011e12343~medium.jpg?fbclid=IwAR3ZdMg8E3S4VJW5IRpMuPFzVAAarAB15GmQ6v1qI78j3YVb3jy8HnMiOu4" style="filter: brightness(60%);"><!-- random image -->
                <div class="caption left-align">
                    <h5 class="light grey-text text-lighten-3">Learn more on hurricane and how to prepare for it </h5>
                </div>
            </li>
            <li>
                <img src="https://images-assets.nasa.gov/image/GSFC_20171208_Archive_e000672/GSFC_20171208_Archive_e000672~medium.jpg?fbclid=IwAR3W14hPc-gUPpqILQWEgmJO67pbGC2Cnqj3vLZytNrJK19ZycvPTEBH6t8" style="filter: brightness(50%);">
                <!-- random image -->

                <div class="caption left-align">
                    <h5 class="light grey-text text-lighten-3">Learn more on hurricane and how to prepare for it </h5>
                </div>

            </li>
        </ul>
    </div>




<div class="container">
    <div class="section">

        <!--   Icon Section   -->
        <div class="row">
            <div class="col s12 m4">
                <div >
                    <h2 class="center "><img src="https://spaceplace.nasa.gov/hurricanes/en/hurricane_diagram_large.en.jpg" alt="Hurricane formation" width="200" height="170"></h2>
                    <h5 class="center white-text">What is a Hurricane?</h5>

                    <p class="light white-text"><font size="2.5">Hurricanes are large, swirling storms. They produce winds of 119 kilometers per hour (74 mph) or higher. Once a hurricane forms, weather forecasters predict its path. They also predict how strong it will get. This information helps people get ready for the storm.</font></p>
                </div>
            </div>

            <div class="col s12 m4">
                <div class="icon-block">
                    <h2 class="center a"><img src="https://www.nasa.gov/sites/default/files/hurricane_3.jpg" width="200" height="170"></h2>
                    <h5 class="center white-text">How strong a hurricane can be?</h5>

                    <p class="light white-text"><font size="2.5">Category 1: Winds 119-153 km/hr (74-95 mph) - faster than a cheetah
                        Category 2: Winds 154-177 km/hr (96-110 mph) - as fast or faster than a baseball pitcher's fastball
                        Category 3: Winds 178-208 km/hr (111-129 mph) - similar, or close, to the serving speed of many professional tennis players
                        Category 4: Winds 209-251 km/hr (130-156 mph) - faster than the world's fastest roller coaster
                            Category 5: Winds more than 252 km/hr (157 mph) - similar, or close, to the speed of some high-speed trains.</font></p>
                </div>
            </div>

            <div class="col s12 m4">
                <div class="icon-block">
                    <h2 class="center a"><img src="https://www.nasa.gov/sites/default/files/hurricane_1.jpg" width="200" height="170"></h2>
                    <h5 class="center white-text">How to prepare for a hurricane?</h5>

                    <p class="light white-text" ><font size="2.5">Take Action
                            Just follow this easy 4-steps process to build your own emergency plan!
                            Gather Information
                            Know if you live in an evacuation area. Find out what type of emergencies could occur and how you should respond.
                            Prepare a supplies Kit
                            Put together a basic disaster supplies kit and consider storage locations for different situations. Help community members do the same.
                            Evacuation
                            FOLLOW instructions issued by local officials. Leave immediately if ordered!
                            Consider your protection options to decide whether to stay or evacuate your home if you are not ordered to evacuate.
                            Recover
                            Wait until an area is declared safe before returning home.
                            Remember that recovering from a disaster is usually a gradual process.
                        </font></p>
                </div>
            </div>
            <div class="row">
                <h6 align="left"><p>


                        <a  class="btn bg-blue-light" href="https://www.ready.gov/hurricanes">Get more information about how to act in case of hurricanes</a>

                    </p>
                </h6>
            </div>
        </div>

    </div>
    <br><br>
</div>

<?php

$this->load->view("footer"); ?>