<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->view("header");
?>


    <div class="slider">
        <ul class="slides">
            <li>
                <img src="https://images-assets.nasa.gov/image/PIA13950/PIA13950~medium.jpg?fbclid=IwAR1DlOnLOttMxjvEyzgQcGKOrIxITBjjUFfG_XujN8VpyoTs0ddj8jZuMTw" style="filter: brightness(70%);">
                <!-- random image -->
                <div class="caption center-align">

                    <h1 class="header center">Earthquake</h1>
                    <div class="row center">
                        <h5 class="header col s12 light">Learn more on earthquake and how to prepare for it</h5>
                    </div>
                    <br><br>
                </div>
            </li>
            <li>
                <img src="https://images-assets.nasa.gov/image/PIA10772/PIA10772~small.jpg?fbclid=IwAR1gchSgZC0giWcahXJAkqmvhaUkf3qv999DrYd3BiE08qn23Mr36VidlJs" style="filter: brightness(50%);">
                <!-- random image -->
                <div class="caption center-align">

                    <h1 class="header center">Earthquake</h1>
                    <div class="row center">
                        <h5 class="header col s12 light">Learn more on earthquake and how to prepare for it</h5>
                    </div>
                    <br><br>
                </div>
            </li>
        </ul>
    </div>




<div class="container">
    <div class="section">

        <!--   Icon Section   -->
        <div class="row">
            <div class="col s12 m4">
                <div >
                    <h2 class="center "><img src="https://blogs.nasa.gov/disaster-response/wp-content/uploads/sites/275/2017/09/MEXICO-landslides.jpg" alt="Earthquake formation" width="200" height="170"></h2>
                    <h5 class="center white-text">What is an Earthquake?</h5>

                    <p class="light white-text"><font size="3">An earthquake (also known as a quake, tremor or temblor) is the shaking of the surface of the Earth, resulting from the sudden release of energy in the Earth's lithosphere that creates seismic waves. Earthquakes can range in size from those that are so weak that they cannot be felt to those violent enough to toss people around and destroy whole cities. The seismicity, or seismic activity, of an area is the frequency, type and size of earthquakes experienced over a period of time. The word tremor is also used for non-earthquake seismic rumbling.
                        </font></p>
                </div>
            </div>

            <div class="col s12 m4">
                <div class="icon-block">
                    <h2 class="center a"><img src="https://images-assets.nasa.gov/image/0004352/0004352~medium.jpg" alt="destruction by earthquake" width="200" height="170"></h2>
                    <h5 class="center white-text">How to measure an earthquake?</h5>

                    <p class="light white-text"><font size="3">Every tremor produces different types of seismic waves, which travel through rock with different velocities:
                            Longitudinal P-waves (shock- or pressure waves)
                            Transverse S-waves (both body waves)
                            Propagation velocity of the seismic waves ranges from approx. 3 km/s up to 13 km/s, depending on the density and elasticity of the medium. Earthquakes are not only categorized by their magnitude but also by the place where they occur. The world is divided into 754 Flinn–Engdahl regions (F-E regions), which are based on political and geographical boundaries as well as seismic activity.
                            Standard reporting of earthquakes includes its magnitude, date and time of occurrence, geographic coordinates of its epicenter, depth of the epicenter, geographical region, distances to population centers, location uncertainty, a number of parameters that are included in USGS earthquake reports, and a unique event ID.
                        </font></p>
                </div>
            </div>

            <div class="col s12 m4">
                <div class="icon-block">
                    <h2 class="center a"><img src="https://www.jpl.nasa.gov/images/uavsar/20090616/radar-20090616-browse.jpg" width="200" height="170"></h2>
                    <h5 class="center white-text">How to prepare for an Earthquake?</h5>

                    <p class="light white-text" ><font size="3">Earthquake insurance can provide building owners with financial protection against losses resulting from earthquakes.

                            Stock up now on emergency supplies that can be used after an earthquake. These supplies should include a first aid kit, survival kits for the home, automobile, and workplace, and emergency water and food. Store enough supplies to last at least 3 days.
                            First Aid Kit
                            Store your first aid supplies in a tool box or fishing tackle box so they will be easy to carry and protected from water. Inspect your kit regularly and keep it freshly stocked. NOTE: Important medical information and most prescriptions can be stored in the refrigerator, which also provides excellent protection from fires.

                        </font></p>
                </div>
            </div>
            <div class="row">
                <h6 align="left"><p>


                            <a  class="btn bg-blue-light" href="https://www.ready.gov/earthquakes">Get more information about how to act in case of earthquakes</a>
                        </font>

                    </p>
                </h6>
            </div>
        </div>

    </div>
    <br><br>
</div>


<?php

$this->load->view("footer"); ?>