<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->view("header");
?>

<div class="slider">
    <ul class="slides">
        <li>
            <img src="https://images-assets.nasa.gov/image/PIA10975/PIA10975~medium.jpg?fbclid=IwAR0xWOZV7E01sLVdqmedQDG0L3OR2D0_KcJPL3fFIaqtH0dLa4ySmCRzavk" style="filter: brightness(70%);">
            <!-- random image -->
            <div class="caption center-align">

                <h1 class="header center">Forest Fire</h1>
                <div class="row center">
                    <h5 class="header col s12 light">Learn more on Forest Fire and how to prepare for it</h5>
                </div>
                <br><br>
            </div>
        </li>
        <li>
            <img src="https://images-assets.nasa.gov/image/PIA20734/PIA20734~medium.jpg?fbclid=IwAR1E-VB7QPxP1vdiHuF2uA3Jtk9fFfeEkc_Im1WqoMTmPhASD7VPQTNBLn8" style="filter: brightness(50%);">
            <!-- random image -->
            <div class="caption center-align">

                <h1 class="header center">Forest Fire</h1>
                <div class="row center">
                    <h5 class="header col s12 light">Learn more on Forest Fire and how to prepare for it</h5>
                </div>
                <br><br>
            </div>
        </li>
        <li>
            <img src="https://images-assets.nasa.gov/image/0201906/0201906~medium.jpg?fbclid=IwAR0gfaKf9DYvkh2wtkyhdpLDu-Y3aucRq_uhQeRNcx-wlA9ekwLtpMM3lvg" style="filter: brightness(60%);"><!-- random image -->
            <div class="caption center-align">

                <h1 class="header center">Forest Fire</h1>
                <div class="row center">
                    <h5 class="header col s12 light">Learn more on Forest Fire and how to prepare for it</h5>
                </div>
                <br><br>
            </div>
        </li>
        <li>
            <img src="https://images-assets.nasa.gov/image/PIA11218/PIA11218~medium.jpg?fbclid=IwAR2cgi2-M1XYuYsk7Bodwa5cjl0zytuFrFBqyzJY3O_lKXF9Dm-opWgVK4k" style="filter: brightness(50%);">
            <!-- random image -->

            <div class="caption center-align">

                <h1 class="header center">Forest Fire</h1>
                <div class="row center">
                    <h5 class="header col s12 light">Learn more on Forest Fire and how to prepare for it</h5>
                </div>
                <br><br>
            </div>

        </li>
    </ul>
</div>





<div class="container">
    <div class="section">

        <!--   Icon Section   -->
        <div class="row">
            <div class="col s12 m4">
                <div >
                    <h2 class="center "><img src="https://images-assets.nasa.gov/image/PIA18054/PIA18054~medium.jpg" alt="Forest Fire"  width="200" height="170"></h2>
                    <h5 class="center white-text">What is Forest Fire?</h5>

                    <p class="light white-text"><font size="2.5">A wildfire is an unplanned, unwanted fire burning in a natural area, such as a forest, grassland, or prairie. As building development expands into these areas, homes and business may be situated in or near areas susceptible to wildfires. This is called the wildland urban interface. Wildfires can damage natural resources, destroy homes, and threaten the safety of the public and the firefighters who protect forests and communities.</font></p>
                </div>
            </div>

            <div class="col s12 m4">
                <div class="icon-block">
                    <h2 class="center a"><img src="https://images-assets.nasa.gov/image/iss007e11614/iss007e11614~medium.jpg" width="220" height="170"></h2>
                    <h5 class="center white-text">How strong wildfire can be?</h5>

                    <p class="light white-text"><font size="2.5">Federal suppression costs typically range from $1 billion to nearly $2 billion each year.1 The destruction caused by wildfires depends on the size of the fire, the landscape, the amount of fuel—such as trees and structures—in the path of the fire, and the direction and intensity of the wind.
                            Wildfires can cause death or injury to people and animals.
                            - Structures may be damaged or destroyed.
                            - Transportation, gas, power, communications, and other services may be disrupted.
                            - Flying embers can set fire to buildings more than a mile away from the wildfire itself.
                            - Smoke can cause health issues for people, even for those far away from the fire.
                            - Extensive acreage can be burned, damaging watersheds and critical natural areas.
                            - Flash flooding and mudslides can result from fire damage to the surrounding landscape.
                            - Wildfires can affect the land for many years, including causing changes to the soil that increase the risk of future floods.</font></p>
                </div>
            </div>

            <div class="col s12 m4">
                <div class="icon-block">
                    <h2 class="center a"><img src="https://images-assets.nasa.gov/image/iss024e015121/iss024e015121~medium.jpg" width="220" height="170"></h2>
                    <h5 class="center white-text">How to prepare for wildfire?</h5>

                    <p class="light white-text" ><font size="2.5">Take Action
                            Just follow this easy 4-steps process to build your own emergency plan!
                            Gather Information
                            Know if you live in an evacuation area. Find out what type of emergencies could occur and how you should respond.
                            Prepare a supplies Kit
                            Put together a basic disaster supplies kit and consider storage locations for different situations. Help community members do the same.
                            EVACUATE
                            When a wildfire threatens your area, the best action to protect yourself and your family is to evacuate early to avoid being trapped. If there is smoke, drive carefully because visibility may be reduced. Keep your headlights on and watch for other vehicles and fleeing wildlife or livestock.
                            DEFENSIBLE SPACE AND FIRE-RESISTANT MATERIALS
                            Your goal now, before a fire happens, is to make your home or business and the surrounding area more resistant to catching fire and burning. This means reducing the amount of material that can burn easily in and around your home or business by clearing away debris and other flammable materials, and using fire-resistant materials for landscaping and construction.
                            INSURANCE
                            Review your homeowners or renters insurance policy to ensure that you have adequate coverage for your property and personal belongings.
                        </font></p>
                </div>
            </div>
            <div class="row">
                <h6 align="left"><p>


                        <a  class="btn bg-blue-light" href="https://www.ready.gov/wildfires">Get more information about how to act in case of Fire</a>

                    </p>
                </h6>
            </div>
        </div>
    </div>
    <br><br>
</div>

<?php

$this->load->view("footer"); ?>
