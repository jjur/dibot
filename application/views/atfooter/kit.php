<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->view("header");
?>

<br xmlns="http://www.w3.org/1999/html"><br>
<h1 class="header center">Emergency Kit</h1>
<div class="row">
    <div class="container col-sm-12">
            <font size="4"><p>An emergency kit is a package of basic tools and supplies prepared in advance to help you survive a crisis. If you find yourself facing a natural disaster, you probably will not be able to go out to buy water or to call a pizza place to order something to eat. Besides lack of access to water and food, you might face loss of power, or even run out of medications. You should have an emergency kit prepared at home with the following items:</font><br>
        <font size="3"><br>
            <b>Documentations:</b><br>
        A copy of passport<br>
        Proof of address<br>
        Copy of medical list and pertinent medical information<br>
        Copy of insurance policies<br>
        Copy of birth certificates<br>
        Family and emergency contact information<br>
        Have copies of all of the above documents saved electronically as well<br>
        <br>
            <b>Basic needs:</b><br>
        Water - one gallon of water per person per day for at least three days, for drinking and sanitation<br>
        Food - at least a three-day supply of non-perishable food<br>
        Moist towelettes, garbage bags and plastic ties for personal sanitation and hygiene<br>
        Extra cash<br>
        Prescription and non-prescription medications (7-day supply)<br>
            <br>

            <b>Tools and equipments:</b>
        Battery-powered or hand crank radio (a NOAA Weather Radio if possible)<br>
        Flashlight<br>
        Extra batteries<br>
        Whistle to signal for help<br>
        Dust mask to help filter contaminated air and plastic sheeting and duct tape to shelter-in-place<br>
        Wrench or pliers to turn off utilities<br>
        Manual can opener for food<br>
        Cell phone chargers and a backup battery<br>
        Local maps<br>
        First aid kit<br>
          <br>
            <b>Additionals -- Consider adding the following items to your emergency supply kit based on your individual needs:</b><br>
        Glasses and contact lense solution<br>
        Infant formula, bottles, diapers, wipes, diaper rash cream<br>
        Pet food and extra water for your pet<br>
        Sleeping bag or warm blanket for each person<br>
        Complete change of clothing appropriate for your climate and sturdy shoes<br>
        Household chlorine bleach and medicine dropper to disinfect water<br>
        Fire extinguisher<br>
        Matches in a waterproof container<br>
        Feminine supplies and personal hygiene items<br>
        Mess kits, paper cups, plates, paper towels and plastic utensils<br>
        Paper and pencil<br>
            Books, games, puzzles or other activities for children<br>
            <br>
            <b>Maintaining Your Kit</b><br>
            After assembling your kit remember to maintain it so it’s ready when needed:<br>

            Keep canned food in a cool, dry place<br>
            Store boxed food in tightly closed plastic or metal containers<br>
            Replace expired items as needed<br>
            Re-think your needs every year and update your kit as your family’s needs change.<br>
            <br>
            <b>Kit Storage Locations<br></b>
            Since you do not know where you will be when an emergency occurs, prepare supplies for home, work and vehicles.<br>

            <b>Home:</b> Keep this kit in a designated place and have it ready in case you have to leave your home quickly. Make sure all family members know where the kit is kept.<br>
            <b>Work:</b> Be prepared to shelter at work for at least 24 hours. Your work kit should include food, water and other necessities like medicines, as well as comfortable walking shoes, stored in a “grab and go” case.<br>
            <b>Vehicle:</b> In case you are stranded, keep a kit of emergency supplies in your car.</font>
                        </p>
    </div>

</div>

<br><br>
<?php

$this->load->view("footer"); ?>
