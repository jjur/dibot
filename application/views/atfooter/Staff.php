<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->view("header");
?>
<br><br>
<h1 class="header center orange-text">STAFFs</h1>
<div class="row center">
    <h5 class="header col s12 light">Members that make Dibot possible!</h5>
</div>

<br><br>

<div class="container">
        <div class="row center">
            <div class="col s12 m12">
                <h6 class="header">Athitaya Plengpojjanart</h6>
                <div class="card horizontal">
                    <div class="card-image">
                        <img src="<?php echo base_url(); ?>/assets/img/staff/xiuxiu.png" width="150" height="190" alt="Xiuxiu">
                    </div>
                    <div class="card-stacked">
                        <div class="card-content">
                            <p align="left"><font color="black">
                                    Background: Thailand<br>
                                School: Minerva Schools at KGI<br>
                                What I like: Cats, clothing, and food!
                                </font>
                            </p>
                        </div>
                        <div class="card-action">
                            <a href="https://www.linkedin.com/in/athitaya-plengpojjanart-274746171/">Learn more about me</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col s12 m12">
                <h6 class="header">Xinyi Ji</h6>
                <div class="card horizontal">
                    <div class="card-image">
                        <img src="<?php echo base_url(); ?>/assets/img/staff/xinyi.png" width="150" height="190" alt="Xinyi">
                    </div>
                    <div class="card-stacked">
                        <div class="card-content">
                            <p align="left"><font color="black">
                                    Background: China<br>
                                    School: Minerva Schools at KGI<br>
                                    What I like: Marine biology and ecology, Violin, and singing<br>
                                </font></p>
                        </div>
                        <div class="card-action">
                            <a href="https://www.linkedin.com/in/欣怡-季-0b5b32152/">Learn more about me</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col s12 m12">
                <h6 class="header">Rafaela Costa</h6>
                <div class="card horizontal">
                    <div class="card-image">
                        <img src="<?php echo base_url(); ?>/assets/img/staff/Rafa.jpg" width="150" height="190" alt="Rafa">
                    </div>
                    <div class="card-stacked">
                        <div class="card-content">
                            <p align="left"><font color="black">
                                    Background: Brazil<br>
                                    School: Minerva Schools at KGI<br>
                                    What I like: Space exploration, sports, and games<br>
                                </font></p>
                        </div>
                        <div class="card-action">
                            <a href="https://www.linkedin.com/in/rafaela-costa-aa8570172/">learn more about me</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col s12 m12">
                <h6 class="header">Peter Moung</h6>
                <div class="card horizontal">
                    <div class="card-image">
                        <img src="<?php echo base_url(); ?>/assets/img/staff/Peter.jpg" width="150" height="190" alt="Xinyi">
                    </div>
                    <div class="card-stacked">
                        <div class="card-content">
                            <p align="left"><font color="black">
                                    Background:Cambodian<br>
                                    Work: Software Test Engineer at Keeper Security, Inc.<br>
                                    What I like:Coding, sports, strategy games and meeting new people<br>
                                </font></p>
                        </div>
                        <div class="card-action">
                            <a href="https://www.linkedin.com/in/peter-moung-32b167a8/">Learn more about me</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col s12 m12">
                <h6 class="header">Angela Chan</h6>
                <div class="card horizontal">
                    <div class="card-image">
                        <img src="<?php echo base_url(); ?>/assets/img/staff/IMG_3369.jpg" width="150" height="190" alt="Xinyi">
                    </div>
                    <div class="card-stacked">
                        <div class="card-content">
                            <p align="left"><font color="black">
                                    School:University of California, Berkeley<br>
                                    What I like: Video games, K-pop, and Zebras
                                </font></p>
                        </div>
                        <div class="card-action">
                            <a href="#">Learn more about me</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col s12 m12">
                <h6 class="header">Juraj Vasek</h6>
                <div class="card horizontal">
                    <div class="card-image">
                        <img src="<?php echo base_url(); ?>/assets/img/staff/Juraj.jpg" width="150" height="190" alt="Juraj">
                    </div>
                    <div class="card-stacked">
                        <div class="card-content">
                            <p align="left"><font color="black">
                                    Background: Slovakia<br>
                                    School: Minerva Schools at KGI<br>
                                    What I like: Coding, nature, electric scooters, and everything from Xiaomi<br>
                                </font></p>
                        </div>
                        <div class="card-action">
                            <a href="https://www.linkedin.com/in/juraj-vasek-844a2890/">Learn more about me</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col s12 m12">
                <h6 class="header">Rohan Jhunjhunwala</h6>
                <div class="card horizontal">
                    <div class="card-image">
                        <img src="<?php echo base_url(); ?>/assets/img/staff/img_1.png" width="150" height="190" alt="Rohan">
                    </div>
                    <div class="card-stacked">
                        <div class="card-content">
                            <p align="left"><font color="black">
                                    Background: First Generation, Indian<br>
                                    School: University of California, Berkeley<br>
                                    What I like: Distance Running, Recreational Mathematics/Puzzles/Programming<br>
                                </font></p>
                        </div>
                        <div class="card-action">
                            <a href="https://rjhunjhunwala.github.io/PersonalWebsite/">Learn more about me</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-copyright black">
        <div class="container">
            Made by <a class="orange-text text-lighten-3" href="http://materializecss.com">Materialize</a>
        </div>
    </div>