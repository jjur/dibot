<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<footer class="page-footer grey darken-4">
    <div class="container">
        <div class="row">
            <div class="col l10 s12">
                <h5 class="white-text white-text">DiBot</h5>
                <p class="grey-text text-lighten-4 white-text">A friend when emergency strikes</p>
                <p class="grey-text text-lighten-4 white-text">All images on this website are from NASA galeries:<br>
                    https://images.nasa.gov/<br>
                    https://www.nasa.gov/mission_pages/neowise/main/index.html<br>
                    https://appliedsciences.nasa.gov/programs/disasters-program<br>
                    https://svs.gsfc.nasa.gov/
                </p>
            </div>
            <div class="col l2 s12">
                <ul>
                    <li><a class="white-text" href="<?php echo base_url(); ?>/Welcome/staff">About Us</a></li>
                    <li><a class="white-text" href="<?php echo base_url(); ?>/Users/Plan">My Account</a></li>
                    <li><a class="white-text" href="<?php echo base_url(); ?>/Welcome/register">Registration</a></li>
                    <li><a class="white-text" href="tel:+17016383998">Contact</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="footer-copyright black">
        <div class="container">
            Made by <a class="bg-blue-text" href="https://2018.spaceappschallenge.org/challenges/volcanoes-icebergs-and-asteroids-oh-my/dont-forget-can-opener/teams/dibot/project">DiBot Team @ NASA Space App Challenge 2018</a>
        </div>
    </div>
</footer>


<!--  Scripts-->
<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<script src="<?php echo base_url(); ?>/assets/js/materialize.js"></script>
<script src="<?php echo base_url(); ?>/assets/js/init.js"></script>
<script>
    document.addEventListener('DOMContentLoaded', function () {
        var elems = document.querySelectorAll('.slider');
        var instances = M.Slider.init(elems, options);
    });

    // Or with jQuery

    $(document).ready(function () {
        $('.slider').slider();
        $(".dropdown-trigger").dropdown();
        M.updateTextFields();
    });
</script>
</body>
</html>