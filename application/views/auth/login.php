<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->view("header");
?>
    <div class="container">
        <h3 class="center">Login</h3>
        <div class="row">
            <div class="col s12">
                <?php echo form_open("auth/login"); ?>
                <div class="card white ">
                    <div class="card-content">
                        <p class="blue-text text-darken-3 center"><?php echo lang('login_subheading'); ?></p>

                            <div id="" class=" blue-text text-darken-4 "><?php echo $message; ?></div>


                            <div class="row">
                                <div class="input-field col s12">
                                    <input id="identity" type="email" name="identity" class="validate">
                                    <label for="identity">Email</label>
                                </div>

                                <div class="input-field col s12">
                                    <input id="password" type="password" name="password" class="validate">
                                    <label for="password">Password</label>
                                </div>
                            </div>

                           <p align="center">
                               <label>
                                   <input type="checkbox" name="remember" id = "remember" value="1" />
                                   <span>Remember me</span>
                               </label>

                            </p>
<br>
                        <center><button class="btn waves-effect waves-light center" type="submit" name="action">Login
                            <i class="material-icons right">send</i>
                        </button></center>




                            <p align="center"><a href="forgot_password"><?php echo lang('login_forgot_password'); ?></a></p>

                    </div>
                </div><?php echo form_close(); ?>
            </div>
        </div>
    </div>


<?php $this->load->view("footer");
?>