
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->view("header");
?>
<div class="container">
    <h3 class="center"><?php echo lang('forgot_password_heading');?></h3>
    <div class="row">
        <div class="col s12">
            <?php echo form_open("auth/forgot_password");?>
            <div class="card white ">
                <div class="card-content white-text">
                    <p class="blue-text text-darken-3 center"><?php echo lang('login_subheading'); ?></p>

                    <div id="infoMessage"><?php echo $message; ?></div>


                    <div class="row">
                        <div class="input-field col s12">
                            <input id="identity" type="email" name="identity" class="validate">
                            <label for="identity">Email</label>
                        </div>
                    </div>

                    <br>
                    <center><button class="btn waves-effect waves-light center" type="submit" name="action">Reset password
                            <i class="material-icons right">send</i>
                        </button></center>



                </div>
            </div><?php echo form_close(); ?>
        </div>
    </div>
</div>


<?php $this->load->view("footer");
?>


