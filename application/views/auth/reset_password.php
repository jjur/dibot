<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->view("header");
?>
    <div class="container">
        <h3 class="center">Reset password</h3>
        <div class="row">
            <div class="col s12">
                <?php echo form_open('auth/reset_password/' . $code);?>
                <div class="card white ">
                    <div class="card-content white-text">
                        <p class="blue-text text-darken-3 center"><?php echo lang('login_subheading'); ?></p>

                        <div id="infoMessage"><?php echo $message; ?></div>


                        <div class="row">
                            <div class="input-field col s12">
                                <input id="new_password" type="password" name="new_password" class="validate">
                                <label for="new_password">Password</label>
                            </div>

                            <div class="input-field col s12">
                                <input id="new_password_confirm" type="password" name="new_password_confirm" class="validate">
                                <label for="new_password_confirm">Password</label>
                            </div>
                        </div>

                        <?php echo form_input($user_id);?>
                        <?php echo form_hidden($csrf); ?>
                        <br>
                        <center><button class="btn waves-effect waves-light center" type="submit" name="action">Login
                                <i class="material-icons right">send</i>
                            </button></center>


                    </div>
                </div><?php echo form_close(); ?>
            </div>
        </div>
    </div>

<?php $this->load->view("footer");
?>
