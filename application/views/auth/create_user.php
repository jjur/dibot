<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->view("header");
?>
<div class="container">
    <h3 class="center">Registration</h3>
    <div class="row">
        <div class="col s12">
            <?php echo form_open("Welcome/Registration_process"); ?>
            <div class="card white ">
                <div class="card-content ">

                    <div id="infoMessage" class=" blue-text text-darken-4 "><?php echo validation_errors(); ?></div>


                    <div class="row">
                        <div class="input-field col m6 s12">
                            <input id="first_name" type="text" name="first_name" class="validate">
                            <label for="first_name">First Name</label>
                        </div>
                        <div class="input-field col m6 s12">
                            <input id="last_name" type="text" name="last_name" class="validate">
                            <label for="last_name">Last Name</label>
                        </div>
                        <div class="input-field col s12">
                            <input id="phone" type="text" name="phone" class="validate">
                            <label for="phone">Mobile phone number (important) +1##########</label>
                        </div>
                        <div class="input-field col s12">
                            <input id="email" type="email" name="email" class="validate">
                            <label for="email">Email</label>
                        </div>

                        <div class="input-field col s12">
                            <input id="password" type="password" name="password" class="validate">
                            <label for="password">Password</label>
                        </div>

                        <div class="input-field col s12">
                            <input id="password_confirm" type="password" name="password_confirm" class="validate">
                            <label for="password_confirm">Repeat password</label>
                        </div>
                    </div>

                    <!--<p align="center">
                        <label>
                            <input type="checkbox" name="remember" id="remember" value="1"/>
                            <span>Remember me</span>
                        </label>

                    </p>-->
                    <br>
                    <center>
                        <button class="btn waves-effect waves-light center" type="submit" name="action">Register
                            <i class="material-icons right">send</i>
                        </button>
                    </center>


                </div>
            </div><?php echo form_close(); ?>
        </div>
    </div>
</div>


<?php $this->load->view("footer");
?>