<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->view("header");
?>


    <br>
    <h1 class="header center">Emergency checklists</h1>
    <div class="row center">
        <h5 class="header col s12 light">Build your own personalized checklist and emergency contacts, in case you will
            need it.</h5>
    </div>

    <br>


    <div class="container">
        <div class="section">
            <div class="row">
                <div class="col m6 s12">

                    <h4>Checklist for Flood</h4>
                    <ul class="collection with-header">

                        <?php foreach ($list1 as $row){ ?>
                        <li class="collection-item blue-text text-darken-4">

                            <div><?php echo $row->i_name;?>
                                <a href="<?php echo base_url(); ?>/Users/delete_item/<?php echo $row->i_id;?>" class="secondary-content"><i class="material-icons">clear</i></a>
                                <!--<a href="#!" class="secondary-content"><i class="material-icons">vertical_align_bottom</i></a>
                                <a href="#!" class="secondary-content"><i class="material-icons">vertical_align_top</i></a>-->
                            </div>
                        </li><?php } ?>

                        <li class="collection-item blue-text text-darken-4"><form id="form1" method="post" action="<?php echo base_url(); ?>/Users/add_item/1">
                            <div  class="input-field"><input id="item1" name="item" type="text" class="validate">
                                    <label for="item1">Add checklist item</label>
                                <a href="#" onclick="form1.submit();" class="secondary-content"><i class="material-icons">add</i></a>
                            </div></form>
                        </li>
                    </ul>
<br>
                    <h4>Checklist for Storms/Huricanes</h4>
                    <ul class="collection with-header">

                        <?php foreach ($list2 as $row){ ?>
                            <li class="collection-item blue-text text-darken-4">

                            <div><?php echo $row->i_name;?>
                                <a href="<?php echo base_url(); ?>/Users/delete_item/<?php echo $row->i_id;?>" class="secondary-content"><i class="material-icons">clear</i></a>
                                <!--<a href="#!" class="secondary-content"><i class="material-icons">vertical_align_bottom</i></a>
                                <a href="#!" class="secondary-content"><i class="material-icons">vertical_align_top</i></a>-->
                            </div>
                            </li><?php } ?>

                        <li class="collection-item blue-text text-darken-4"><form id="form2" method="post" action="<?php echo base_url(); ?>/Users/add_item/2">
                                <div  class="input-field"><input id="item2" name="item" type="text" class="validate">
                                    <label for="item2">Add checklist item</label>
                                    <a href="#" onclick="form2.submit();" class="secondary-content"><i class="material-icons">add</i></a>
                                </div></form>
                        </li>
                    </ul>
                    <br>

                    <h4>Checklist for Earthquake</h4>
                    <ul class="collection with-header">

                        <?php foreach ($list3 as $row){ ?>
                            <li class="collection-item blue-text text-darken-4">

                            <div><?php echo $row->i_name;?>
                                <a href="<?php echo base_url(); ?>/Users/delete_item/<?php echo $row->i_id;?>" class="secondary-content"><i class="material-icons">clear</i></a>
                                <!--<a href="#!" class="secondary-content"><i class="material-icons">vertical_align_bottom</i></a>
                                <a href="#!" class="secondary-content"><i class="material-icons">vertical_align_top</i></a>-->
                            </div>
                            </li><?php } ?>

                        <li class="collection-item blue-text text-darken-4"><form id="form3" method="post" action="<?php echo base_url(); ?>/Users/add_item/3">
                                <div  class="input-field"><input id="item3" name="item" type="text" class="validate">
                                    <label for="item3">Add checklist item</label>
                                    <a href="#" onclick="form3.submit();" class="secondary-content"><i class="material-icons">add</i></a>
                                </div></form>
                        </li>
                    </ul>
                    <br>

                    <h4>Checklist for Fire</h4>
                    <ul class="collection with-header">

                        <?php foreach ($list4 as $row){ ?>
                            <li class="collection-item blue-text text-darken-4">

                            <div><?php echo $row->i_name;?>
                                <a href="<?php echo base_url(); ?>/Users/delete_item/<?php echo $row->i_id;?>" class="secondary-content"><i class="material-icons">clear</i></a>
                                <!--<a href="#!" class="secondary-content"><i class="material-icons">vertical_align_bottom</i></a>
                                <a href="#!" class="secondary-content"><i class="material-icons">vertical_align_top</i></a>-->
                            </div>
                            </li><?php } ?>

                        <li class="collection-item blue-text text-darken-4"><form id="form4" method="post" action="<?php echo base_url(); ?>/Users/add_item/4">
                                <div  class="input-field"><input id="item4" name="item" type="text" class="validate">
                                    <label for="item4">Add checklist item</label>
                                    <a href="#" onclick="form4.submit();" class="secondary-content"><i class="material-icons">add</i></a>
                                </div></form>
                        </li>
                    </ul>
                    <br>


                </div>

                <div class="col m6 s12">

                    <p>Do you have waterproof bags?<br>
                        Do you have a copy of important documentations in a safe place?<br>
                        Are you an immigrant? Do you have your Visa documentation in the kit? <br>
                        Do you have a copy of your ID in the kit?<br>
                        How many people are you preparing for? Do you have enough supply for everyone?<br>
                        Are you preparing for children?<br>
                        What is your gender? (hygiene kit, feminine supplies)<br>
                        Do you have pets? If yes, have you planed for your pets?<br>
                        Do you need special assistance?(e.g. Disability, elderly)<br>
                        Have you prepared financially?(e.g.Backups of financial data)<br>
                        Have you prepared clothes for extreme whether?<br>
                        Are the shoes you prepared sturdy and comfortable enough?<br>

                    </p>
                </div>
            </div>
        </div>
    </div>

<?php

$this->load->view("footer"); ?>