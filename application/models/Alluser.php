<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Alluser extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function get_name_by_phone($phone){
        $result = $this->db->select("*")->where(array("phone"=>$phone))->get("users");
        if ($result->num_rows() >= 1) {
            return $result->row()->first_name;
        } else {
            return FALSE;
        }
    }

    public function get_id_by_phone($phone){
        $result = $this->db->select("*")->where(array("phone"=>$phone))->get("users");
        if ($result->num_rows() >= 1) {
            return $result->row()->id;
        } else {
            return FALSE;
        }
    }

    public function add_item($data)
    {
        return $this->db->set($data)->insert("items");
        # do databazy uloží riadok s dátami a vloží ho do tabuľky users
    }

    public function get_all_items($user, $acitivity){
        $result = $this->db->select("*")->where(array("i_owner"=>$user, "i_type" =>$acitivity))->get("items");
        return $result->result();


    }

    public function delete_item($id){
        $this->db->where(array("i_id" => $id ))->delete("items");
    }


}