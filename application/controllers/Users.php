<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        if (!$this->ion_auth->logged_in()) { #if not logged in, log in
            redirect("Auth");
        }
        $this->load->model("Alluser");
        $id=$this->ion_auth->user();
        $id=$id->row()->id;
        $this->dat_id = $id;
    }

	public function Plan()
	{
	    $DATA["list1"]=$this->Alluser->get_all_items($this->dat_id ,1);
        $DATA["list2"]=$this->Alluser->get_all_items($this->dat_id ,2);
        $DATA["list3"]=$this->Alluser->get_all_items($this->dat_id ,3);
        $DATA["list4"]=$this->Alluser->get_all_items($this->dat_id ,4);
        $this->load->view("user/Lists", $DATA);
	}

	public function index(){

	    redirect("Users/Plan");
    }
    public function staff(){

        $this->load->view('atfooter/Staff');
    }

    public function add_item($type){
        $data = array("i_name" => $this->input->post("item"), "i_owner" => $this->dat_id, "i_type"=>$type);
        $this->Alluser->add_item($data);
        redirect("Users");
}
    public function delete_item($id){
        $this->Alluser->delete_item($id);
        redirect("Users");

    }

    public function demodata($id){



    }

}
