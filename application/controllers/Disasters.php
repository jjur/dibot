<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Disasters extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function Hurricane()
	{
		$this->load->view('disaster/Hurricane');
	}

    public function Earthquake()
    {
        $this->load->view('disaster/Earthquake');
    }

    public function Fire()
    {
        $this->load->view('disaster/Fire');
    }
    public function Flood()
    {
        $this->load->view('disaster/flood');
    }

    public function hello(){

	    $this->load->view("hello");
    }
    public function kit(){
	    $this->load->view('atfooter/kit');
    }
}
