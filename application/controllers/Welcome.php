<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller
{

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *        http://example.com/index.php/welcome
     *    - or -
     *        http://example.com/index.php/welcome/index
     *    - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function index()
    {
        $this->load->view("hello");
    }

    public function hello()
    {

        $this->load->view("hello");
    }

    public function staff()
    {

        $this->load->view('atfooter/Staff');
    }

    public function register()
    {

        $this->load->helper("form");
        $this->load->library("form_validation");
        $this->load->view('auth/create_user');
    }

    public function registration_process()
    {

        $this->load->helper("form");
        $this->load->library('form_validation');

        $this->form_validation->set_rules('password', "Password", 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[password_confirm]');
        $this->form_validation->set_rules('email', "E-mail", 'required|valid_email');
        $this->form_validation->set_rules('password_confirm', "Repeat Password", 'required');
        $this->form_validation->set_rules('phone', "Phone", 'required');
        $this->form_validation->set_rules('first_name', "Fist Name", 'required');
        $this->form_validation->set_rules('last_name', "Last Name", 'required');
        if ($this->form_validation->run() == true) {
            $email = strtolower($this->input->post('email'));
            $identity = $email;
            $password = $this->input->post('password');
            $additional_data = array("first_name" => $this->input->post("first_name"),
                "last_name" => $this->input->post("last_name"),
                "phone" => $this->input->post("phone")
            );
        }
        if ($this->form_validation->run() == true && $this->ion_auth->register($identity, $password, $email,$additional_data)) {
            $DATA["message"] = "Registration completed succesfully!";
            $this->load->view("auth/login", $DATA);
            //header("Location: " . base_url() . "/Info/prihlasenie?mainterence=0");
        } else {
            $this->load->view("auth/create_user");
            // display the create user form
            // set the flash data error message if there is one
            //echo (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
        }

    }

}
