<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller
{

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *        http://example.com/index.php/welcome
     *    - or -
     *        http://example.com/index.php/welcome/index
     *    - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function index()
    {

        $my_array = array("hello", "world");
        echo json_encode($my_array);

    }


    public function get_intro()
    {
        $this->load->model("Alluser");
        $phone = $this->input->post("UserIdentifier");
        //$phone = "+16289994432";
        $name = $this->Alluser->get_name_by_phone($phone);
        $json = <<<EOT
        {
	"actions": [
		{
			"say": "Hello $name! I'm your virtual emergency assistant. What would you like help with today?"
		},
		{
			"listen": true
		}
	]
}
EOT;
        echo $json;

    }


    public function get_list()
    {
        $josn = <<<EOT
{
  "actions": [
    {
      "say": "This is answer from API server. The connection has been established successfully. Please enjoy the development and continue in coding. You still have to code tautens of features!"
    }
  ]
}
EOT;
    }

    public function no()
    {
        $josn = <<<EOT
{
  "actions": [
    {
			"say": "Can you repeat it one more time please?"
		},
		{
			"listen": true
		}
  ]
}
EOT;
        echo $josn;
    }

    public function check_remember()
    {

        $phone = json_decode($this->input->post("Memory"));


        $phone = $phone->emergency;
        $json = <<<EOT
        {
	"actions": [
		{
			"say": "Ok, i heard  $phone, am I right?"
		},
		{
			"listen": true
		}
	]
}
EOT;
        echo $json;

    }

    public function yes()
    {

        $this->load->model("Alluser");
        $memory = json_decode($this->input->post("Memory"));
        $phone = $this->input->post("UserIdentifier");
        if ($phone == "") {
            $id = 0;
        } else {
            $id = $this->Alluser->get_id_by_phone($phone);
        }


        $emergency = $memory->emergency;
        $message = "Ok, I am sorry to hear that. Please calm down. 911 is being notified. We will go through our checklist. Let's start with ";
        switch ($emergency) {

            case "fire":
                $opts = $this->Alluser->get_all_items($id, 4);
                foreach ($opts as $opt) {
                    $message = $message . ", " . $opt->i_name. ".  .  .    ";
                }
                break;


            case "earthquake":
                $opts = $this->Alluser->get_all_items($id, 3);
                foreach ($opts as $opt) {
                    $message = $message . ", " . $opt->i_name. ".  .  .    ";
                }
                break;

            case "hurricane":
                $opts = $this->Alluser->get_all_items($id, 2);
                foreach ($opts as $opt) {
                    $message = $message . ", " . $opt->i_name. ".  .  .    ";
                }
                break;

            case "flood":
                $opts = $this->Alluser->get_all_items($id, 1);
                foreach ($opts as $opt) {
                    $message = $message . ", " . $opt->i_name. ".  .  .    ";
                }
                break;

        }


        $json = <<<EOT
        {
	"actions": [
		{
			"say": "$message"
		},
		{
			"listen": true
		}
	]
}
EOT;
        echo $json;


    }


}
