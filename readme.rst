###################
What is DiBot
###################

It is a project for NASA Space Apps Challenge 2018.
The challenge is: "Don't forget the can opener!" https://2018.spaceappschallenge.org/challenges/volcanoes-icebergs-and-asteroids-oh-my/dont-forget-can-opener/
The Team Profile: https://2018.spaceappschallenge.org/challenges/volcanoes-icebergs-and-asteroids-oh-my/dont-forget-can-opener/teams/dibot/project


DiBot is our virtual emergency assistant, is focused on assisting you during an emergency. It will help you in a step by step process to evacuate to a safe location and give instructions about what to do in a specific situation.

*******************
DEMO
*******************
Feel free to call at:
+ 1 (701) 638-3998
And use http://dibot.ml
to customize your emergency checklist.



*******************
Used technology
*******************

- PHP (CodeIgniter 3.1.9)
- HTML&CSS (MaterializeCSS 1.0.0)
- MySQL database
- Twilio Autopilot https://www.twilio.com/autopilot

******************
More details
******************

The first step to face an emergency is to be prepared! Go to our website (http://dibot.ml) and get information about the four most common types of natural disasters in the United States and how to act during them. Then, register on our platform and follow our checklist to prepare the emergency kit that you will use in a crisis. Finally, save our “Bot” number in your cell phone contact list so you are to ready to call it when you face a disaster.
Our virtual assistant number: (701) 638-3998

******************
Disclaimer
******************
This technology is under development and at the moment cannot replace emergency calls like 911 or 112.
